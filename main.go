package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

var data = `
images:
  - major: 11
    minor: 14
    patch: 0
  - major: 11
    minor: 5
    patch: 0
  - major: 11
    minor: 4
    patch: 0
  - major: 10
    minor: 9
    patch: 0
  - major: 9
    minor: 9
    patch: 0
  - major: 8
    minor: 9
    patch: 4
`

type Images struct {
	Images []Image `yaml:"images"`
}

type Image struct {
	Major int `yaml:"major"`
	Minor int `yaml:"minor"`
	Patch int `yaml:"patch"`
}

func main() {
	images := Images{}

	err := yaml.Unmarshal([]byte(data), &images)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	tplBytes, err := ioutil.ReadFile("./Dockerfile.tpl")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	tplS := string(tplBytes)
	tpl, err := template.New("docker").Parse(tplS)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, e := range images.Images {
		fmt.Println(e)

		err := os.MkdirAll(fmt.Sprintf("./dockerfiles/v%d.%d.%d", e.Major, e.Minor, e.Patch), os.ModePerm)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		file, err := os.Create(fmt.Sprintf("./dockerfiles/v%d.%d.%d/Dockerfile", e.Major, e.Minor, e.Patch))

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		err = tpl.Execute(file, e)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	tplBytes, err = ioutil.ReadFile("./.gitlab-ci.yml.tpl")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	tplS = string(tplBytes)
	tpl, err = template.New("gitlab").Parse(tplS)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	file, err := os.Create("./.gitlab-ci.yml")

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = tpl.Execute(file, images)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
