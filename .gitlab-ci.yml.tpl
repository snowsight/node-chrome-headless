image: node:8

stages:
  - build-images

.build:
  stage: build-images
  services:
    - docker:dind
  image: docker:17
  script:
    - echo $DOCKER_PASSWORD | docker login -u $DOCKER_USERNAME --password-stdin
    - docker build --pull -t snowsight/node-chrome-headless:$VER ./dockerfiles/$VER
    - docker push snowsight/node-chrome-headless:$VER



{{ range $image := .Images }}
build-v{{$image.Major}}.{{$image.Minor}}.{{$image.Patch}}:
  extends: .build
  variables:
    VER: v{{$image.Major}}.{{$image.Minor}}.{{$image.Patch}}
{{ end }}