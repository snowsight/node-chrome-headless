FROM debian:latest
LABEL author="Hugo <hugo@snowsight.com>"
LABEL vendor="Snow Sight"
LABEL tools="git chrome node npm yarn"

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    git \
    python-minimal \
    nodejs \
    build-essential \
    --no-install-recommends

# Get Chrome sources
RUN curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list
# Install Chrome
RUN apt-get update && apt-get install -y \
    google-chrome-stable \
    --no-install-recommends
# Get yarn sources
RUN curl -sSL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb [arch=amd64] https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
# Install yarn pinned version
RUN apt-get update && apt-get install -y \
    yarn=1.6.0-1 \
    --no-install-recommends
# Find your desired version here: https://deb.nodesource.com/node_{{.Major}}.x/pool/main/n/nodejs/
RUN curl https://deb.nodesource.com/node_{{.Major}}.x/pool/main/n/nodejs/nodejs_{{.Major}}.{{.Minor}}.{{.Patch}}-1nodesource1_amd64.deb > node.deb
RUN dpkg -i node.deb
RUN rm node.deb